<?php

namespace spec\ChessRatingTools;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FideXmlReaderSpec extends ObjectBehavior
{
    const FILE = "./spec/ChessRatingTools/players_list_xml_test.xml";

    function it_is_initializable()
    {
        $this->beConstructedWith(self::FILE);
        $this->shouldHaveType('ChessRatingTools\FideXmlReader');
    }

    function it_search_by_id()
    {
        $this->beConstructedWith(self::FILE);
        $this->searchById('5716365')->shouldContain('A Hamid, Harman');
    }

    function it_search_by_name()
    {
        $this->beConstructedWith(self::FILE);
        $this->searchByName('Moh')->shouldHaveCount(2);
    }
}
