<?php

namespace spec\ChessRatingTools;

use PhpSpec\ObjectBehavior;

/**
 * Class RatingCalculatorSpec
 * @package spec\Chess
 * @mixin \ChessRatingTools\RatingCalculator
 */
class RatingCalculatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ChessRatingTools\RatingCalculator');
    }

	function it_calc_player_simple_performance()
	{
		$games = [
			[2000 => 1], [0 => 1], [2100 => 1], [2000 => 0.5], [2300 => 0]
		];

        $this->setGames($games);
		$this->basicPerformance()->shouldReturn(2200);
	}

    function it_return_zero_performance_with_no_games()
    {
        $games = [];

        $this->setGames($games);
        $this->basicPerformance()->shouldReturn(0);
    }

    function it_calc_unrated_player_rating()
    {
        $games = [
            [2220 => 0], [2220 => 0.5], [2220 => 1]
        ];

        $this->setGames($games);
        $this->unratedPlayerRating()->shouldReturn(2220);

        $games = [
            [2220 => 0], [2220 => 0.5], [2220 => 0.5]
        ];

        $this->setGames($games);
        $this->unratedPlayerRating()->shouldReturn(2095);
        // avg = 2184, w = 6.5, n = 12
        $games = [
            [2220 => 0], [2220 => 0], [2220 => 1], [2150 => 1],
            [2150 => 1] ,[2150 => 1], [2150 => 0], [2150 => 0], [2200 => 1],
            [2200 => 0.5], [2200 => 0], [2200 => 1]
        ];

        $this->setGames($games);
        $this->unratedPlayerRating()->shouldReturn(2204);
    }

    function it_calc_rating_variation()
    {
        $this->beConstructedWith([[2200 => 1]], 2100, 15);
        $this->changeRating()->shouldReturn(9.6);

        $this->setGames([[2050 => 0.5]]);
        $this->changeRating()->shouldReturn(-1.05);

        $this->setGames([[2200 => 1], [2050 => 0.5]]);
        $this->changeRating()->shouldReturn(8.55);
    }

	function it_calc_rating_variation_with_extreme_elos()
	{
		$this->beConstructedWith([[2088 => 0]], 1200, 40);
		$this->changeRating()->shouldReturn(-3.2);
	}
}
