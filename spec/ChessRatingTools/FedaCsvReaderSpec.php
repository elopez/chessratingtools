<?php

namespace spec\ChessRatingTools;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FedaCsvReaderSpec extends ObjectBehavior
{
    const FILE = "./spec/ChessRatingTools/FedaSample.csv";

    function it_is_initializable()
    {
        $this->beConstructedWith(self::FILE);
        $this->shouldHaveType('ChessRatingTools\FedaCsvReader');
    }

    function it_search_by_id()
    {
        $this->beConstructedWith(self::FILE);
        $this->searchById('16963')->shouldContain('ABAD ALHAMBRA, LAURA');
    }

    function it_search_by_name()
    {
        $this->beConstructedWith(self::FILE);
        $this->searchByName('ABAD ALHAMBRA')->shouldHaveCount(3);
    }
}
