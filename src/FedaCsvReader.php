<?php
namespace ChessRatingTools;

class FedaCsvReader
{
    const LINES_TO_SKIP = 4;

    protected $filePath;
    private $csvReader;

    /**
     * @param string $file Path of CSV file
     *
     */
    public function __construct($file)
    {
        $this->filePath = $file;
    }

    /**
     * Get next player of CSV
     *
     * @return \Generator
     */
    public function getPlayers()
    {
        // Open CSV
        $this->csvReader = fopen($this->filePath, "r");

        // Skip 4 first lines
        for ($f = 1; $f <= self::LINES_TO_SKIP; $f++) {
            $row = fgetcsv($this->csvReader, 1024);
        }

        while($row = fgetcsv($this->csvReader, 1024)) {
            $player = [
                "fedaid" => (int) $row[0],
                "name" => $row[1],
                "federation" => $row[2],
                "rating" => $row[3],
                "games" => $row[4],
                "birthday" => $row[5],
                "old_rating" => (int) $row[8],
            ];

            yield $player;
        }
    }

    public function searchById($id)
    {
        foreach($this->getPlayers() as $player) {
            if ($player['fedaid'] == $id) {
                return $player;
            }
        }
    }

    public function searchByName($name)
    {
        $players = [];
        foreach($this->getPlayers() as $player) {
            if (preg_match('/'.$name.'/i', $player['name'])) {
                $players[] = $player;
            }
        }

        return $players;
    }
}
