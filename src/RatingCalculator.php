<?php

namespace ChessRatingTools;

class RatingCalculator
{
    protected $playerRating;
    protected $games;
    protected $constantK;

    /**
     * @param array $games
     * @param int $playerRating
     * @param int $k
     */
    public function __construct($games = [], $playerRating = 0, $k = 15)
    {
        $this->games = $games;
        $this->playerRating = $playerRating;
        $this->constantK = $k;
    }

    /**
     * @param array $games
     */
    public function setGames($games = [])
    {
        $this->games = $games;
    }

    /**
     * @param int $playerRating
     */
    public function setPlayerRating($playerRating)
    {
        $this->playerRating = $playerRating;
    }

    /**
     * @param int $k
     */
    public function setConstantK($k)
    {
        $this->constantK = $k;
    }

    /**
     * @return int
     */
    public function basicPerformance()
    {
    	$total = array();

        foreach ($this->games as $game) {
        	if (! key($game)) continue;

        	if (current($game) == 1) $plus = 400;
        	elseif (current($game) == 0) $plus = -400;
        	else $plus = 0;

        	$total[] = (key($game) + $plus);
        }

        return count($total) > 0
            ? (int) round(array_sum($total) / count($total))
            : 0;
    }

    /**
     * @return int
     */
    public function unratedPlayerRating()
    {
        $ratings = [];
        $points = [];

        foreach ($this->games as $game) {
            $ratings[] = key($game);
            $points[] = current($game);
        }

        $avgElo = round(array_sum($ratings) / count($ratings));
        $points = array_sum($points);
        $percentage = $points / count($ratings);

        if ($percentage == 0.50) return (int) round($avgElo);
        if ($percentage < 0.50) return (int) $avgElo + $this->ratingGap($percentage);

        $pointsOver50Percentage = abs(count($ratings) / 2 - $points);
        return (int) ($avgElo + ($pointsOver50Percentage / 0.5 * 20));
    }

    /**
     * It calc the player's rating change after N games
     * @return float
     */
    public function changeRating()
    {
        $variation = 0;

        foreach ($this->games as $game) {
            if (! key($game)) continue;
            $expected = $this->expectedPercentage($this->playerRating, key($game));
            $result = current($game);

            $variation += ($result - $expected) * $this->constantK;
        }
        return round($variation, 2);
    }

    /**
     * Having a player's rating and the opponent's rating, it calc the expected percentage
     * @param $ratingPlayer int
     * @param $ratingOpponent int
     * @return float
     */
    protected function expectedPercentage($ratingPlayer, $ratingOpponent)
    {
        $percentages = [
            0, 4, 11, 18, 26, 33, 40, 47, 54, 62, 69, 77, 84, 92, 99,
            107, 114, 122, 130, 138, 146, 154, 163, 171, 180, 189,
            198, 207, 216, 226, 236, 246, 257, 268, 279, 291, 303, 316,
            329, 345, 358, 375, 392, 412, 433, 457, 485, 518, 560, 620,
            735];

        $diff = $ratingPlayer - $ratingOpponent;

        if ($diff > 400) $diff = 400;
        if ($diff < -400) $diff = -400;


        $lastGood = 50;
        foreach ($percentages as $k => $percentage) {
            if (abs($diff) < $percentage) {
                return ($diff > 0 ? $lastGood : 100 - $lastGood) / 100;
            }
            $lastGood = 50 + $k;
        }

        return $lastGood;
    }

    /**
     * Having a percentage, it calc the rating gap
     * @param $percentage float
     * @return int
     */
    protected function ratingGap($percentage)
    {
        $percentages = [
            800, 677, 589, 538, 501, 470, 444, 422, 401, 383, 366, 351, 336, 322, 309,
            296, 284, 273, 262, 251, 240, 230, 220, 211, 202, 193, 184, 175, 166,
            158, 149, 141, 133, 125, 117, 110, 102, 95, 87, 80, 72, 65, 57, 50, 43,
            36, 29, 21, 14, 7, 0
        ];

        $percentage = (int) round($percentage * 100);

        if ($percentage >= 50) return $percentages[100 - $percentage];

        return (int) $percentages[$percentage] * -1;
    }
}
