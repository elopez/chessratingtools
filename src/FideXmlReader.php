<?php

namespace ChessRatingTools;

/**
 * Class FideXmlReader
 *
 * This class can parse the Fide Rating XML (https://ratings.fide.com/download.phtml)
 *
 * @package ChessRatingTools
 */
class FideXmlReader
{
    /**
     * @var string Path to the XML file
     */
    protected $file;

    /**
     * @var string current XML tag
     */
    protected $element;

    /**
     * @var array Current player info
     */
    protected $player = [];

    /**
     * @var array Store some player parsed by xml_parse
     */
    protected $players = [];

    /**
     * @param string $file Path to XML Fide Rating
     */
    public function __construct($file)
    {
        $this->file = $file;
        $this->player = $this->getCleanPlayer();
    }

    /**
     * @param string $fideId
     *
     * @return array|null
     */
    public function searchById($fideId) {
        $selected = null;

        foreach ($this->getPlayers() as $player) {
            if ($player['fideid'] == $fideId) {
                $selected = $player;
                break;
            }
        }

        return $selected;
    }

    /**
     * @param string[] $fideIds
     *
     * @return array|null
     */
    public function searchByIds($fideIds) {
        $founds = [];

        foreach ($this->getPlayers() as $player) {
            if (in_array($player['fideid'], $fideIds)) {
                $founds[$player['fideid']] = $player;
                $fideIds = array_diff($fideIds, [$player['fideid']]);

                if (empty($fideIds)) {
                    break;
                }
            }
        }

        return $founds;
    }

    /**
     * @param string $name
     *
     * @return array Players that match with $name
     */
    public function searchByName($name, $onlyFirst = false) {
        $selected = [];
        foreach ($this->getPlayers() as $player) {
            if (preg_match("/$name/i", $player['name'])) {
                $selected[] = $player;

                if ($onlyFirst) {
                    break;
                }
            }
        }

        return $selected;
    }

    /**
     * Fetch a player from XML file
     *
     * @return \Generator
     * @throws \Exception
     */
    public function getPlayers()
    {
        $parser = xml_parser_create();

        xml_set_element_handler($parser, [$this, 'startElements'], [$this, 'endElements']);
        xml_set_character_data_handler($parser, [$this, 'dataElements']);

        if (!($handle = fopen($this->file, "r"))) {
            throw new \Exception("could not open XML input");
        }

        while ($data = fread($handle, 4096)) {
            // Fix HHVM problem
            if (substr($data, -1, 1) != "\n") {
                $data .= fgets($handle);
            }

            xml_parse($parser, $data, feof($handle));

            foreach ($this->players as $player) {
                yield $player;
            }

            $this->players = [];
        }

        xml_parser_free($parser);
    }

    protected function startElements($parser, $name, $attrs)
    {
        $this->element = $name;
    }

    protected function endElements($parser, $name)
    {
        if ($name == 'PLAYER') {
            $this->players[] = $this->player;

            $this->player = $this->getCleanPlayer();
            $this->element = null;
        }
    }

    protected function dataElements($parser, $data)
    {
        $data = trim($data);

        if (!$data) {
            return;
        }

        $this->player[strtolower($this->element)] = $data;
    }

    protected function getCleanPlayer()
    {
        return [
            "fideid" => null,
            "name" => null,
            "country" => null,
            "rating" => null,
            "k" => null,
            "birthday" => null,
            "sex" => null,
            "title" => null,
            "w_title" => null,
            "o_title" => null,
            "games" => 0,
            "rapid_rating" => null,
            "rapid_games" => null,
            "rapid_k" => null,
            "blitz_rating" => null,
            "blitz_games" => null,
            "blitz_k" => null,
            "flag" => 0
        ];
    }
}
