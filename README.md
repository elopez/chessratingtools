Chess Rating Tools
========================

Some PHP helpers classes about chess rating.

Rating Calculator
-----------------

    <?php

        use ChessRatingTools\RatingCalculator;

        $games = [[2100 => 1], [2050 => 0.5], [0 => 1]]; // [opponent's rating => result]
        $myRating = 2100;
        $k = 20; // K is the development coefficient, http://www.fide.com/component/handbook/?id=172&view=article Point: 8.56.

        $calculator = new RatingCalculator($games, $myRating, $k);

        echo $calculator->changeRating();
        echo $calculator->basicPerformance();
        echo $calculator->unratedPlayerRating();

    ?>

Fide Rating File XML Parser
---------------------------

    <?php

        use ChessRatingTools\FideXmlReader;
        
        $reader = new  FideXmlReader('./players_list_xml.xml');
        
        // All players with name like '*Carlsen*'
        $carlsens = $reader->searchByName('Carlsen');
        
        // Search a player by ID
        $kasparov = $reader->searchById('4100018');
        
        // Search by multiples IDs
        $players = $reader->searchByIds(['4100018', '123456']);
        
        // Players with > 2700 rating
        foreach ($reader->getPlayers() as $player) {
            if ($player['rating'] > 2700) {
                printf("%s [%d]\n", $player['name'], $player['rating']);
            }
        }
    ?>

Feda Rating CSV File Parser
---------------------------
You can create The file .csv saving the first page of feda.xls. Download from:
http://www.feda.org/web/index.php/elo

    <?php

        use ChessRatingTools\FedaCsvReader;
        
        $reader = new FedaCsvReader('./feda_201509.csv');
        
        // Search a player by ID
        $player = $reader->searchById(12345);
    ?>
